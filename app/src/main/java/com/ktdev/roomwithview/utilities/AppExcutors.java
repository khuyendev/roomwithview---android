package com.ktdev.roomwithview.utilities;

import com.ktdev.ApplicationLoader;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class AppExcutors {
    private static ExecutorService IO_EXECUTOR = Executors.newSingleThreadExecutor();

    public static void runOnIoThread(Runnable runnable) {
        IO_EXECUTOR.execute(runnable);
    }

    public static void runOnUIThread(Runnable runnable) {
        runOnUIThread(runnable, 0);
    }

    public static void runOnUIThread(Runnable runnable, long delay) {
        if (delay == 0) {
            ApplicationLoader.applicationHandler.post(runnable);
        } else {
            ApplicationLoader.applicationHandler.postDelayed(runnable, delay);
        }
    }

    public static void cancelRunOnUIThread(Runnable runnable) {
        ApplicationLoader.applicationHandler.removeCallbacks(runnable);
    }
}
