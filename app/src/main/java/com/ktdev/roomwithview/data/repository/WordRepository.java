package com.ktdev.roomwithview.data.repository;

import android.app.Application;
import android.os.AsyncTask;

import com.ktdev.roomwithview.data.WordRoomDatabase;
import com.ktdev.roomwithview.data.database.dao.WordDao;
import com.ktdev.roomwithview.data.database.entity.Word;
import com.ktdev.roomwithview.utilities.AppExcutors;

import java.util.List;

import androidx.lifecycle.LiveData;

public class WordRepository {
    private WordDao mWordDao;
    private LiveData<List<Word>> mAllWords;

    public WordRepository(Application application) {
        WordRoomDatabase db = WordRoomDatabase.getDatabase(application);
        mWordDao = db.wordDao();
        mAllWords = mWordDao.getAllWords();
    }

    public LiveData<List<Word>> getAllWords() {
        return mAllWords;
    }

    public void insert(final Word word) {
        AppExcutors.runOnIoThread(new Runnable() {
            @Override
            public void run() {
                mWordDao.insert(word);
            }
        });
    }

    public void delete(final Word word) {
        AppExcutors.runOnIoThread(new Runnable() {
            @Override
            public void run() {
                mWordDao.delete(word);
            }
        });
    }
}
