package com.ktdev.roomwithview;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.ktdev.roomwithview.ui.word.WordFragment;

public class WordActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.word_activity);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, WordFragment.newInstance())
                    .commitNow();
        }
    }
}
